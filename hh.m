%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Integration of Hodgkin--Huxley equations with Euler method
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clf;
% maximal conductances (in units of mS/cm^2); 1=K, 2=Na, 3=L
g(1)=36; g(2)=120; g(3)=0.3;
% battery voltage ( in mV); 1=K, 2=Na, 3=L
E(1)=-12; E(2)=115; E(3)=10.613;
% Initialization of external current (in \mu A/cm^2)
I_ext=0; %EXTERNAL CURRENT
% Initialization of membrane potential
V=0;
% Initialization of n,m,h=x(1),x(2),x(3)
% These are the equilibrium values for I_ext=0
% You can check this by changing these values for x, running the
% HH equations which will go to a fixed point after one transient spike and
% observing the asymptotic values of x
x=[ 0.3177 0.0530 0.5960];
% Time step for integration
dt=0.01;
% Index for collecting values as a function of time
i=0;
% Maxium time in msec
t_max=80;
% Integration with Euler method
for t=1:dt:t_max
	i=i+1;
% The function tau_n, tau_m and tau_h in eq. 13, 15 and 16
% depend on voltage. Their plots are given in fig. 33
% We define tau_n, tau_m and tau_h as tau(i), i=1,2,3
% Hodgkin and Huxley defined them as tau(i)=1/(alpha(i)+beta(i))
% with
	alpha(1)=(10-V)/(100*(exp((10-V)/10)-1));
	alpha(2)=(25-V)/(10*(exp((25-V)/10)-1));
	alpha(3)=0.07*exp(-V/20);
	
	beta(1)=0.125*exp(-V/80);
	beta(2)=4*exp(-V/18);
	beta(3)=1/(exp((30-V)/10)+1);
	
	tau=1./(alpha+beta);
%The function n_\infty, m_\infty and h_\infty in eq. 13, 15 and 16
%depend on voltage. Their plots are given in fig. 33
%We define n_\infty, m_\infty and h_\infty as x_0(i), i=1,2,3
%In terms of alpha and beta they are x_0(i)=alpha(i)*tau(i)
	x_0=alpha.*tau;
% leaky integration with Euler method eq. 13, 15, 16:
	x=x+dt./tau.*(x_0-x);
% calculate actual conductances g with given n, m, h
gnmh(1)=g(1)*x(1)^4; % g_K(t)=g_K n^4 eq. 12
gnmh(2)=g(2)*x(2)^3*x(3); % g_N(t)=g_N m^3 h eq. 14
gnmh(3)=g(3); % g_L(t)=constant
% update voltage of membrane
V=V+dt*(I_ext-gnmh*(V-E)');
V_all(i)=V;
end % time loop
plot(1:dt:t_max,V_all); xlabel('Time'); ylabel('Voltage');
